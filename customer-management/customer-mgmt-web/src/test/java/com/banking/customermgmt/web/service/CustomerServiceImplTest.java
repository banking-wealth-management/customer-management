package com.banking.customermgmt.web.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.banking.customermgmt.web.model.Customer;
import com.banking.customermgmt.web.repository.CustomerRepo;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerServiceImplTest {

	@Autowired
	CustomerRepo repo;

	Customer customer;

	@Before
	public void setup() {
		customer = new Customer();
	}

	@Test
	public void testSaveCustomer() {
		this.repo.save(customer);
		assertNotNull(this.repo.findById(1l)); 
	}

	
}
