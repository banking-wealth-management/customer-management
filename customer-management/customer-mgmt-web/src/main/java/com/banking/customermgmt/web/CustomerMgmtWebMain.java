package com.banking.customermgmt.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerMgmtWebMain {
	public static void main(String[] args) {
		SpringApplication.run(CustomerMgmtWebMain.class, args);
	}
}
