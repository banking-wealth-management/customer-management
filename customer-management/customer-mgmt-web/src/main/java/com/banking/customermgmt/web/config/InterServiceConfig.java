package com.banking.customermgmt.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
//@EnableRetry
public class InterServiceConfig {

	@Value("${interservice.url.portfolioapi:http://localhost:7072/api/v1/services/portfolio}")
	private String portfolioApiUrl;

	@Value("${interservice.url.fundstrackerapi:http://localhost:7073/api/v1/services/fundstracker}")
	private String fundsTrackerApiUrl;

//	@Bean(name = "OAuth2ClientContext_default")
//	public OAuth2ClientContext createAuth2ClientContext() {
//		return new DefaultOAuth2ClientContext();
//	}
//
//	@Bean
//	public OAuth2RestTemplate creatAauth2RestTemplate(OAuth2ProtectedResourceDetails auth2ProtectedResourceDetails,
//			@Qualifier("OAuth2ClientContext_default") OAuth2ClientContext context) {
//		return new OAuth2RestTemplate(auth2ProtectedResourceDetails, context);
//	}
}
