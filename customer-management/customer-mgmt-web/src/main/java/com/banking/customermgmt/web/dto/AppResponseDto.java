package com.banking.customermgmt.web.dto;

import java.util.List;

import com.banking.customermgmt.web.model.BaseModel;

public class AppResponseDto {
	private boolean result = Boolean.TRUE;
	private BaseModel model;
	List<BaseModel> models;

	public AppResponseDto(BaseModel model) {
		this.model = model;
	}

	public AppResponseDto() {
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public BaseModel getModel() {
		return model;
	}

	public void setModel(BaseModel model) {
		this.model = model;
	}

	public List<BaseModel> getModels() {
		return models;
	}

	public void setModels(List<BaseModel> models) {
		this.models = models;
	}

}
