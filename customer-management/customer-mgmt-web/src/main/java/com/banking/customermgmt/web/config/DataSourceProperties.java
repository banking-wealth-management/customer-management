package com.banking.customermgmt.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceProperties {

	//@Value("${appdb.url}")
	private String appdbUrl;

	//@Value("${appdb.username}")
	private String userName;

	//@Value("${appdb.password}")
	private String password;

	public String getAppdbUrl() {
		return appdbUrl;
	}

	public void setAppdbUrl(String appdbUrl) {
		this.appdbUrl = appdbUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
