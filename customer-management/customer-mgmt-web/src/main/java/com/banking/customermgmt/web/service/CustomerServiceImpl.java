package com.banking.customermgmt.web.service;

import org.springframework.stereotype.Service;

import com.banking.customermgmt.web.model.Customer;
import com.banking.customermgmt.web.repository.CustomerRepo;

@Service
public class CustomerServiceImpl implements CustomerService {

	CustomerRepo repo;

	public CustomerServiceImpl(CustomerRepo repo) {
		this.repo = repo;
	}

	@Override
	public Customer getCutomerDetails(Long id) {
		return this.repo.findById(id).orElse(null);
	}

}
