package com.banking.customermgmt.web.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banking.customermgmt.web.dto.AppResponseDto;
import com.banking.customermgmt.web.service.CustomerService;

@RestController
@RequestMapping(path = "/api/v1/services/customer")

public class CustomerApi {
	private static Logger logger = LoggerFactory.getLogger(CustomerApi.class);

	@Autowired
	CustomerService service;

//trace < debug < info < warn < error
	@GetMapping(path = "getcustomerdetails/{id}")
	public ResponseEntity<AppResponseDto> getCustomerDetails(@PathVariable Long id) {
		logger.info("Enter");
		ResponseEntity<AppResponseDto> result = ResponseEntity.ok(new AppResponseDto(service.getCutomerDetails(id)));

		logger.info("Exit");
		return result;
	}
}
