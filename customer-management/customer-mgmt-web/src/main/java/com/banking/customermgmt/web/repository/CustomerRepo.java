package com.banking.customermgmt.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banking.customermgmt.web.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {

}
